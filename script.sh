#!/bin/bash

wget -O server.jar "https://yivesmirror.com/files/paper/PaperSpigot-$1-R0.1-SNAPSHOT-latest.jar"

echo $"$#!/bin/sh 

while true
do
    java -Xms$2 -Xmx$3 -jar server.jar
    pause

    sleep 4
    for i in 3 2 1
    do 
    
        echo \"$i...\"
        sleep 1
    done
    echo \"Server Restarting...\"
done"> run.sh

chmod +x run.sh

./run.sh